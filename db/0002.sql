/**
平台基础表初始化数据
 */

insert into user set u_name = 'admin', name='默认管理员',email='zhangtao54@gmail.com', salt='27ad28b980affe8c453e4f65fadcba45624b6a01', crypted_password='a96cdd2daff763cbab9028f2730d83a6cd5ddb93', created_at = now();
insert into permission(name, description, created_at, updated_at) values
  ("system.mgmt", "系统管理", now(), now()),
  ("operator.mgmt", "业务管理", now(), now()));

insert into role(name, description, updated_at, created_at) values
  ("普通用户", "", now(), now()),
  ("管理员", "", now(), now());

insert into role_permission(role_id, permission_id, updated_at, created_at) values
  (1, 1, now(), now()),
  (2, 1, now(), now()),
  (2, 2, now(), now()),
  (2, 3, now(), now());

insert into user_role(user_id, role_id, updated_at, created_at) values
  (1, 1, now(), now()),
  (1, 2, now(), now());