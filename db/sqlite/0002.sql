/**
平台基础表初始化数据
 */
insert into user(name,u_name,email,salt,crypted_password) values('admin','默认管理员','zhangtao54@gmail.com','27ad28b980affe8c453e4f65fadcba45624b6a01','a96cdd2daff763cbab9028f2730d83a6cd5ddb93');
insert into permission(name, description, created_at, updated_at) values("system.mgmt", "系统管理", datetime(), datetime());
insert into permission(name, description, created_at, updated_at) values("operator.mgmt", "业务管理", datetime(), datetime());
insert into permission(name, description, created_at, updated_at) values("role.mgmt", "角色管理", datetime(), datetime());
insert into permission(name, description, created_at, updated_at) values("permission.mgmt", "功能管理", datetime(), datetime());

insert into role(name, description, updated_at, created_at) values ("普通用户", "", datetime(), datetime());
insert into role(name, description, updated_at, created_at) values ("管理员", "", datetime(), datetime());


insert into role_permission(role_id, permission_id, updated_at, created_at) values (1, 1, datetime(), datetime());
insert into role_permission(role_id, permission_id, updated_at, created_at) values (2, 1, datetime(), datetime());
insert into role_permission(role_id, permission_id, updated_at, created_at) values (2, 2, datetime(), datetime());
insert into role_permission(role_id, permission_id, updated_at, created_at) values (2, 3, datetime(), datetime());
insert into role_permission(role_id, permission_id, updated_at, created_at) values (2, 4, datetime(), datetime());


insert into user_role(user_id, role_id, updated_at, created_at) values  (1, 1, datetime(), datetime());
insert into user_role(user_id, role_id, updated_at, created_at) values  (1, 2, datetime(), datetime());