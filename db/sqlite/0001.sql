/**
平台基础表
 */

create table user(
  id integer  primary key autoincrement not null,
  name varchar(32) ,
  u_name varchar(32) ,
  email varchar(100),
  salt varchar(64),
  crypted_password varchar(64),
  created_at datetime default (datetime('now', 'localtime'))
);

create table permission(
   id integer  primary key autoincrement not null,
  name varchar(255) not null ,
  description varchar(1024) ,
  updated_at datetime default (datetime('now', 'localtime'))  ,
  created_at datetime default (datetime('now', 'localtime'))
);

create table role(
  id integer  primary key autoincrement not null,
  name varchar(255) not null  ,
  description varchar(1024)  ,
  updated_at datetime default (datetime('now', 'localtime'))   ,
  created_at datetime default (datetime('now', 'localtime'))
);

create table role_permission(
  id integer  primary key autoincrement not null,
  role_id bigint not null  ,
  permission_id bigint not null ,
  updated_at datetime default (datetime('now', 'localtime'))   ,
  created_at datetime default (datetime('now', 'localtime'))
)  ;

create table user_role(
  id integer  primary key autoincrement not null,
  user_id bigint not null  ,
  role_id bigint not null  ,
  updated_at datetime default (datetime('now', 'localtime'))   ,
  created_at datetime default (datetime('now', 'localtime'))
) ;

create table logs(
  id integer  primary key autoincrement not null,
  operator bigint  not null  ,
  action varchar(512)  ,
  operateTime datetime default (datetime('now', 'localtime'))
);

create table dic_type(
   id integer  primary key autoincrement not null,
   code varchar(32),
   name varchar(64)
);

create table dic_data(
  id integer  primary key autoincrement not null,
  type_id integer ,
  code varchar(32),
  name varchar(64)
);