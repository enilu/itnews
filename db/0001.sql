/**
平台基础表
 */
create table user(
  id bigint auto_increment primary key,
  uname varchar(32),
  name varchar(32) comment 'label: 用户名, searchable: eq, validate: length(min=5, max=32)',
  email varchar(100) comment 'label: 邮箱, searchable: like, validate: email',
  salt varchar(64) comment '用于加密的盐',
  crypted_password varchar(64) comment '加密后的密码',
  created_at timestamp not null,
  unique key uk_user_name(name)
) engine=innodb default charset=utf8 comment 'fake_columns: password:string';

create table permission(
  id bigint not null auto_increment primary key,
  name varchar(255) not null comment 'label: 权限标识名，如user.write, user.view, searchable:like',
  description varchar(1024) comment 'label: 权限描述',
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null,
  unique key uk_permission_name(name)
) engine=InnoDB default charset=utf8 comment 'label: 权限';

create table role(
  id bigint not null auto_increment primary key,
  name varchar(255) not null comment 'label: 角色名, searchable:like',
  description varchar(1024) comment 'label: 角色描述',
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null,
  unique key uk_role_name(name)
) engine=InnoDB default charset=utf8 comment 'label: 角色';

create table role_permission(
  id bigint not null auto_increment primary key,
  role_id bigint not null comment 'ref: role',
  permission_id bigint not null comment 'ref: permission',
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null,
  unique key uk_role_permission_role_id_permission_id(role_id, permission_id)
) engine=InnoDB default charset=utf8 comment '角色，权限关联表';

create table user_role(
  id bigint not null auto_increment primary key,
  user_id bigint not null comment 'ref: user',
  role_id bigint not null comment 'ref: role',
  updated_at timestamp not null default current_timestamp on update current_timestamp,
  created_at timestamp not null,
  unique key uk_user_role_user_id_role_id(user_id, role_id)
) engine=InnoDB default charset=utf8 comment '用户,角色关联表';

create table logs(
  id bigint not null auto_increment primary key,
  operator bigint  not null comment 'label:用户',
  action varchar(512) comment 'label:用户操作',
  operateTime timestamp comment 'label:操作日期' ,
) engine=InnoDB default charset=utf8 comment 'label: 日志';

create table dic_type(
   id bigint not null auto_increment primary key,
   code varchar(32) not null comment 'label:code,searchable:eq',
   name varchar(64) not null comment 'label:字典类型, searchable:eq'
) engine=InnoDB default charset=utf8 comment '字典类型';

create table dic_data(
  id bigint not null auto_increment primary key,
  type_id bigint  not null comment 'ref: dic_type',
  code varchar(32) comment '值, searchable:eq',
  name varchar(64) comment '显示值, searchable:eq'
) engine=InnoDB default charset=utf8 comment '字典';