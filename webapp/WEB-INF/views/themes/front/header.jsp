<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>攻城师的新闻列表</title>

	<!-- 新 Bootstrap 核心 CSS 文件 -->
	<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">

	<!-- 可选的Bootstrap主题文件（一般不用引入） -->
	<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">
	<style>
		body {
			padding-top: 50px;
		}
		.starter-template {
			padding: 40px 15px;
			text-align: center;
		}

	</style>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">攻城师的新闻列表</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li id="index" ><a href="/">新闻</a></li>

				<li id="comments"><a href="/comments">评论</a></li>
				<li id="submit"><a href="/submit">提交</a></li>
				<li><a target="_blank" href="http://www.enilu.cn">博客</a></li>
			</ul>
			<c:if test="${currentUser!=null}">
				<div class="navbar-form navbar-right" >
					<a   href="/profile" style="color: white;margin-top: 5px;font-size: large">${currentUser.name}</a>&nbsp;&nbsp;
					<a   href="/sessions/logout" style="color: white;margin-top: 5px;font-size: large">退出</a>
				</div>
			</c:if>
			<c:if test="${currentUser==null}">
			<form class="navbar-form navbar-right"  action="/login" method="post" role="form">
				<div class="form-group">
					<input type="text" placeholder="userName" name="name" class="form-control">
				</div>
				<div class="form-group">
					<input type="password" placeholder="Password" name="password" class="form-control">
				</div>
				<button type="submit" class="btn btn-success">登录</button>
				<a href="/register" class="btn btn-primary">注册</a>
			</form>
			</c:if>
		</div><!--/.navbar-collapse -->
	</div>
</nav>
