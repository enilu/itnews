<jsp:include page="/WEB-INF/views/layouts/header.jsp"/>

<div class="container bodytag" bodytag="index">
    <br>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" action="/sessions/new" method="post" role="form">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">用户名</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" placeholder="用户名/邮箱">
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">密码</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" placeholder="密码">
                    </div>
                </div>

                <%--<div class="form-group">--%>
                    <%--<div class="col-sm-offset-2 col-sm-10">--%>
                        <%--<div class="checkbox">--%>
                            <%--<label>--%>
                                <%--<input type="checkbox"> Remember me--%>
                            <%--</label>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                <%--</div>--%>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Sign </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; enilu 2016</p>
    </footer>
</div> <!-- /container -->


<jsp:include page="/WEB-INF/views/layouts/footer.jsp"/>
