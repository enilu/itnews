<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  uri="http://www.joda.org/joda/time/tags" prefix="joda"%>
<jsp:include page="/WEB-INF/views/layouts/header.jsp"/>


<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h2>${latest.title}</h2>
        <p>${latest.descript}</p>
        <p><a class="btn btn-primary btn-lg" target="_blank" href="${latest.url}" role="button">阅读全文 &raquo;</a></p>

    </div>
</div>
<div class="container bodytag" bodytag="index">

    <!-- Example row of columns -->

    <div class="row">
        <c:forEach items="${news.data}" var="item" varStatus="status">
        <div class="col-md-4">
            <h3><a  target="_blank" href="${item.url}">${item.title}</a></h3>
            <joda:format value="${item.createDate}" pattern="yyyy-MM-dd"></joda:format>&nbsp;&nbsp;
            <a href="/comments/${item.id}" type="button" class="btn btn-default btn-sm" id="btnComment">
                ${item.commentCount}&nbsp;<span class="glyphicon glyphicon-comment"></span>
            </a>

            <%--<button type="button" class="btn btn-default btn-sm">--%>
                <%--<span class="glyphicon glyphicon-star"></span> 收藏--%>
            <%--</button>--%>
            <p>${item.descript}</p>
            <p>


            </p>

        </div>
            <c:if test="${status.count%3==0}">&nbsp;  <hr></c:if>
        </c:forEach>
    </div>

    <hr>

    <footer>
        <p>&copy; enilu 2016</p>
    </footer>
</div> <!-- /container -->


<jsp:include page="/WEB-INF/views/layouts/footer.jsp"/>
