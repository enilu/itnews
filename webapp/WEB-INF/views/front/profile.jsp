<jsp:include page="/WEB-INF/views/layouts/header.jsp"/>

<div class="container">
	<br>
	<div class="row">
		<div class="col-md-12">
			 <table>
				 <tr>
					 <td>用户名</td><td>${user.name}</td>
				 </tr>
				 <tr>
					 <td>电子邮箱</td><td>${user.email}</td>
				 </tr>
				 <tr>
					 <td>注册日期</td><td>${user.createdAt}</td>
				 </tr>

			 </table>
		</div>
	</div>

	<hr>

	<footer>
		<p>&copy; enilu 2016</p>
	</footer>
</div> <!-- /container -->


<jsp:include page="/WEB-INF/views/layouts/footer.jsp"/>
