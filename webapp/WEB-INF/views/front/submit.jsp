<jsp:include page="/WEB-INF/views/layouts/header.jsp"/>

<div class="container bodytag" bodytag="submit">
    <br>
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" action="/submit" method="post" role="form">
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">标题</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title" placeholder="文章标题">
                    </div>
                </div>
                <div class="form-group">
                    <label for="url" class="col-sm-2 control-label">文章链接</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="url" name="url" placeholder="文章链接">
                    </div>
                </div>
                <div class="form-group">
                    <label for="descript" class="col-sm-2 control-label">文章概要或者评论</label>
                    <div class="col-sm-10">
                       <textarea  class="form-control"  rows="10" id="descript" name="descript" placeholder="请输入不少与50字的摘要或评论"></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default"> 提交 </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; enilu 2016</p>
    </footer>
</div> <!-- /container -->


<jsp:include page="/WEB-INF/views/layouts/footer.jsp"/>
