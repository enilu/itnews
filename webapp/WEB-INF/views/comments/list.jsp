<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@taglib  uri="http://www.joda.org/joda/time/tags" prefix="joda"%>
<jsp:include page="/WEB-INF/views/layouts/header.jsp"/>

<div class="container bodytag" bodytag="comments">

    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12" style="margin-top: 15px;">
                <h3><a  target="_blank" href="${news.url}">${news.title}</a></h3>

                <p>${news.descript}</p>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12" >
            <form class="form-horizontal" action="/comments" method="post" role="form">
                <input type="hidden" name="newsId" value="${news.id}"/>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea  class="form-control"  rows="10" id="comment" name="comment" placeholder="请输入评论"></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <div class=" col-sm-2">
                        <button type="submit" class="btn btn-default"> 添加评论 </button>
                    </div>
                </div>
            </form>
        </div>
        </div>
    <div class="row">
        <div class="col-md-12" >
            <table>
            <c:forEach items="${comments}" var="item">
                <tr>
                    <td><c:if test="${item.user==null}">匿名</c:if>${item.user.name}&nbsp;|&nbsp;<joda:format value="${item.createDate}" pattern="yyyy-MM-dd"></joda:format></td>
                </tr>
                <tr>
                    <td>
                        <p>${item.comment}</p>
                    </td>
                </tr>
            </c:forEach>
            </table>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; enilu 2016</p>
    </footer>
</div> <!-- /container -->


<jsp:include page="/WEB-INF/views/layouts/footer.jsp"/>
