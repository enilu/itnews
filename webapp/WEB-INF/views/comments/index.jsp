<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@taglib  uri="http://www.joda.org/joda/time/tags" prefix="joda"%>
<jsp:include page="/WEB-INF/views/layouts/header.jsp"/>

<div class="container bodytag" bodytag="comments">

    <!-- Example row of columns -->

    <div class="row">
        <div class="col-md-12" style="margin-top: 15px;">
            <table>
            <c:forEach items="${comments.data}" var="item">

                <tr>
                    <td><c:if test="${item.user==null}">匿名</c:if> ${item.user.name}&nbsp;|&nbsp;</td>
                    <td><joda:format value="${item.createDate}" pattern="yyyy-MM-dd"></joda:format>&nbsp;|&nbsp;</td>
                    <td>on:<a target="_blank" href="${item.news.url}">${item.news.title}</a></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p>${item.comment}</p>
                    </td>
                </tr>


            </c:forEach>
            </table>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; enilu 2016</p>
    </footer>
</div> <!-- /container -->


<jsp:include page="/WEB-INF/views/layouts/footer.jsp"/>
