# itnews

练习项目，仿hacknews的新闻列表系统

## 启动项目
- 直接运行start.bat/start.sh即可
- 访问http://localhost
- 如果80端口被占用，在start.bat/start.sh更改启动端口即可

#依赖环境
- JDK 1.7以上(自行安装）
- maven3.3(自行安装）
- sqlite嵌入式数据库,项目已包含sqlite数据库文件，位置：(${项目目录}/db/itnews.db
- 开发工具，建议使用Intellij IDEA，eclipse也可以。

# 开发
- 下载webflash项目，mvn install 安装到本地仓库
- 将项目导入开发工具（idea或者eclpse即可）
- 数据库使用sqlitedb，开发环境db放在db/itnews.db,
- config.properties中正确配置slitedb文件位置即可。
- 运行：cn.enilu.flash.web.Main 即可启动项目，默认的端口是8080


# 打包部署

```
$ mvn package
$ mvn -DskipTests clean package
```

- 打包后会在target/生成itnews-${timestamp}.tar.gz 包
- 解压后，根据实际情况更改etc/目录下配置文件即可
- 运行项目：执行 start.bat/start.sh即可。
 