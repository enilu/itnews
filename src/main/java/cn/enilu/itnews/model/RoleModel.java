package cn.enilu.itnews.model;

import cn.enilu.flash.core.lang.Lists;
import cn.enilu.itnews.entity.Role;
import cn.enilu.itnews.entity.Permission;

import java.util.ArrayList;
import java.util.List;

public class RoleModel extends Role {
    
    private List<Permission> permissions;

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }
    
    public List<Long> getPermissionIds() {
        if (permissions == null) {
            return new ArrayList<>();
        }
        return Lists.map(permissions, "id");
    }
}
