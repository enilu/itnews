package cn.enilu.itnews.model;

import cn.enilu.flash.core.db.annotation.EntityReference;
import cn.enilu.itnews.entity.Comment;
import cn.enilu.itnews.entity.News;
import cn.enilu.itnews.entity.User;

/**
 * class CommentModel<br>
 * </p> Copyright by easecredit.com<br>
 * 作者: zhangtao <br>
 * 创建日期: 16-9-7<br>
 */
public class CommentModel extends Comment {
    /**
     * 盘点计划
     */
    @EntityReference(referenceProperty = "newsId")
    private News news;
    @EntityReference(referenceProperty = "authorId")
    private User user;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
