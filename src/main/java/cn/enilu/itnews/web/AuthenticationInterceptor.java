package cn.enilu.itnews.web;

import cn.enilu.flash.web.auth.UserContext;
import cn.enilu.itnews.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * class AuthenticationInterceptor<br>
 * </p> Copyright by easecredit.com<br>
 * 作者: zhangtao <br>
 * 创建日期: 16-9-6<br>
 */
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationInterceptor.class);

    public AuthenticationInterceptor() {
    }

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object user  =   request.getSession().getAttribute("currentUser");
        request.setAttribute("userContext", user);
        request.setAttribute(UserContext.USER_ATTRIBUTE, user);

        return true;

    }
}
