package cn.enilu.itnews.entity;

import org.joda.time.DateTime;

import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;

/**
 * 新闻评论
 */
@Table(name="comments")
public class Comment {

	@Id
	private Integer id;

	@Column
	private Long authorId;
	
	@Column
	private String comment;
	@Column(name = "news_id")
	private Long newsId;

	@Column
	private DateTime createDate = DateTime.now();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public DateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(DateTime createDate) {
		this.createDate = createDate;
	}
}
