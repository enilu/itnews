package cn.enilu.itnews.service;

import cn.enilu.flash.core.db.Pagination;
import cn.enilu.flash.core.db.Query;
import cn.enilu.flash.core.service.EntityService;
import cn.enilu.flash.web.QueryForm;
import cn.enilu.itnews.entity.News;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NewsService extends EntityService<News, Long> {
	private Logger logger = LoggerFactory.getLogger(NewsService.class);

	public NewsService() {
		super(News.class);
	}

	@Transactional
	public Pagination<News> search(QueryForm qf) {
		Query q = db.from("news");
		return q.orderBy("id desc").paginate(News.class, qf.getPage(),9);
	}
	public News getLatest(){
		return db.from(News.class).where("status",1).first(News.class);
	}
}
