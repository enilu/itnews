package cn.enilu.itnews.controller;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/profile")
public class ProfileController extends BaseController {
    @RequestMapping(  method = RequestMethod.GET)
    public String index(HttpServletRequest request){
        Object user = request.getSession().getAttribute("user");
        request.setAttribute("user",user);
        return "/front/profile";
    }
}
