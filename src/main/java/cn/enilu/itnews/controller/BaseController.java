package cn.enilu.itnews.controller;


import cn.enilu.flash.core.db.RecordNotFoundException;
import cn.enilu.flash.web.taglib.Breadcrumb;
import cn.enilu.itnews.web.MenuList;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

public class BaseController extends cn.enilu.flash.web.BaseController {
    private Logger logger = LoggerFactory.getLogger(BaseController.class);

    @ModelAttribute("theme")
    public String getTheme() {
        return "front";
    }//adminLTE simplenso


    @ExceptionHandler(value={Exception.class})
    public String handleException(Exception ex, HttpServletRequest request) {
        logger.error("未处理的异常, url=" + request.getRequestURI(), ex);

        if (ex instanceof RecordNotFoundException || ex instanceof BadRequestException) {
            return "404";
        }

        return "500";
    }
    
    protected void setBreadcrumb(HttpServletRequest request, Breadcrumb breadcrumb) {
        request.setAttribute("breadcrumb", breadcrumb);
    }

    protected void setBreadcrumb(Breadcrumb breadcrumb) {
        getRequest().setAttribute("breadcrumb", breadcrumb);
    }

    protected void setBreadcrumb(HttpServletRequest request, String... nameLinks) {
        setBreadcrumb(request, new Breadcrumb(nameLinks));
    }
    
    protected void setBreadcrumb(String... nameLinks) {
        setBreadcrumb(getRequest(), new Breadcrumb(nameLinks));
    }

    public Long getUserId(){
        HttpServletRequest request =getRequest();
       String uid =  request.getSession().getAttribute("uid").toString();
        return StringUtils.isBlank(uid)? null:Long.parseLong(uid);
    }
}
