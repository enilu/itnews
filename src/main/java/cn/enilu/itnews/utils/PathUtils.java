package cn.enilu.itnews.utils;

import java.io.File;
import java.net.URLDecoder;

/**
 * class PathUtils<br>
 * </p> Copyright by easecredit.com<br>
 * 作者: zhangtao <br>
 * 创建日期: 16-3-23<br>
 */
public class PathUtils {
    public static String workPath;
    public static String webRootPath;
    public static String webInfoPath;
    public static String uploadPath;
    public static String downloadPath;
    public static String templatePath;
//    public static String classesPath;

    static {

        try {
            webRootPath = (new File("webapp")).getAbsolutePath() + "/";
            webRootPath = URLDecoder.decode(webRootPath, "UTF-8");
        } catch (Exception var1) {
            throw new Error("核心工具类PathUtils出错,系统启动失败");
        }

        webRootPath = webRootPath.replace("\\", "/");

        workPath = ((new File(webRootPath)).getParent() + "/").replaceAll("\\\\", "/");

        webInfoPath = webRootPath + "WEB-INF/";
        uploadPath = webRootPath + "upload/";
        templatePath = webInfoPath+"templates/";

        downloadPath =webRootPath+"download/";
//        classesPath = PathUtils.class.getResource("/").getPath();

    }

    private PathUtils() {
    }

    public static void main(String[] args) {
        System.out.println(workPath);
        System.out.println(webRootPath);
//        System.out.println(classesPath);
    }
}
