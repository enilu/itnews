package cn.enilu.itnews.utils;

import cn.enilu.flash.core.lang.Beans;
import cn.enilu.flash.core.lang.Reflects;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

/**
 * 将数据转换为csv的工具类
 * </p> Copyright by easecredit.com<br>
 * 作者: zhangtao <br>
 * 创建日期: 16-8-30<br>
 */
public class CsvUtil {
    /**
     * 根据model类型获取csv头信息
     * @param klass
     * @return
     */
    public static  List<String> getHeader(Class klass){
        List<Field> fields  = Reflects.getFields(klass,false);
        List<String> result = new LinkedList<String>();
        for(int i=0;i<fields.size();i++){
            result.add(fields.get(i).getName());
        }
        return result;
    }

    /**
     * 将一条记录转换为逗号分割的字符串（csv格式）
     * @param headers csv头信息，决定了值的顺序
     * @param entity 要转换的数据记录
     * @return
     */
    public static String getRow(List<String> headers,Object entity){
        StringBuilder builder = new StringBuilder();
        for(int i=0;i<headers.size();i++){
            builder.append(Beans.get(entity,headers.get(i))).append(",");
        }
        String result = builder.toString();
        result = result.substring(0,result.length()-1);
        return result;
    }
}
