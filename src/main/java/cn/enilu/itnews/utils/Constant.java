package cn.enilu.itnews.utils;

/**
 * Created by zhangxue on 2015-11-04.
 */
public class Constant {
    /**
     * 字典表已经被使用过的标识
     */
    public enum DicUsed{
        no("未使用",0),
        yes("已使用",1);


        private String name;
        private Integer value;

        private DicUsed(String name,Integer value){
            this.name = name;
            this.value = value;
        }

        public Integer getValue() {
            return this.value;
        }
        public String getName(){
            return this.name;
        }

    }
    private Constant(){}

    public enum MaterialStatus{
        wait("待入库",0),
        in("已入库",1),
        out("已出库",2);

        private String name;
        private Integer value;

        private MaterialStatus(String name,Integer value){
            this.name = name;
            this.value = value;
        }

        public Integer getValue() {
            return this.value;
        }
        public String getName(){
            return this.name;
        }

    }
    /**
     * 财务报表类型
     */
    public enum CheckStatus{
        no("未盘点",0),
        yes("盘点完毕",1),
        wrong("盘点错误",2),
        ing("盘点中",3);
        private String name;
        private Integer value;

        private CheckStatus(String name,Integer value){
            this.name = name;
            this.value = value;
        }

        public Integer getValue() {
            return this.value;
        }
        public String getName(){
            return this.name;
        }
    }
    public enum Role{
        System("系统测试","0"),
        Admin("管理员","1"),
        Purchaser("采购","2"),
        CustomerService("客服","3"),
        ZhChecker("中文报告审核员","4"),
        TranslateManager("翻译经理","5"),
        Translater("翻译供应商","6"),
        ZhProvider("中文供应商","7"),
        EnCheckMyself("外文自审人员","9"),
        ZhCheckMyself("中文自审人员","8");

        private String name;
        private String value;
        private Role(String name,String value){
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }


}
