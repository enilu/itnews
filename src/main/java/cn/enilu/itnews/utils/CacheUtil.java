package cn.enilu.itnews.utils;

import cn.enilu.flash.core.lang.Maps;

import java.util.Map;

/**
 * 对外提供的缓存接口<br>
 *     测试过程使用map缓存数据，部署到生产环境的时候使用redis缓存
 * </p> Copyright by easecredit.com<br>
 * 作者: zhangtao <br>
 * 创建日期: 16-4-18<br>
 */
public class CacheUtil {
    private  static Map<String,String> map ;
    static {
        map = Maps.newHashMap();
    }
    public static String get(String key) {
        String result = map.get(key);
        return result;
    }

    public static  void  set(String key, String val) {
        map.put(key,val);
    }
}
