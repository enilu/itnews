package cn.enilu.itnews.utils;


/**
 * Created by tongda-kf02 on 2015/11/27.
 */
public class IdUtils {

    public static String createId(int length){
       if(length==0||length<10){
           length = 10;
       }
        String id = "";
        for(int i=0;i<length;i++){
            id+=String.valueOf((int)(Math.random()*10));
        }
        return id;

    }

    public static void main(String[] args){
        System.out.println(createId(10));
    }
}
