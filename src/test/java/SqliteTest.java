/**
 * Created by root on 16-1-16.
 */

import java.sql.*;

/**
 * class SqliteTest<br>
 * </p> Copyright by easecredit.com<br>
 * 作者: zhangtao <br>
 * 创建日期: 16-1-16<br>
 */
public class SqliteTest {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        String jdbcUrl = "jdbc:sqlite:/home/hello.db";
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection(jdbcUrl);
        PreparedStatement preparedStatement = connection.prepareStatement("select * from user");
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            int id = resultSet.getInt(1);
            String name = resultSet.getString(2);
            System.out.println("id="+id+" name="+name);
        }
        resultSet.close();
        preparedStatement.close();
        connection.close();
    }
}
